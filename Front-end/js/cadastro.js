var nomeValor = document.getElementById('nome').value;
var cargoValor = document.getElementById('cargo').value;
var cpfValor = document.getElementById('cpf').value;
var ativoValor;

const verificaCadastro = () => {
    //Codico que comparação que valida se uma string contem somente números
    const numbers = /^[0-9]+$/;

    nomeValor = document.getElementById('nome').value;
    cargoValor = document.getElementById('cargo').value;
    cpfValor = document.getElementById('cpf').value;

    //muda a cor da span de alert para vermelho representando um erro
    document.getElementById("spanAlert").style.color = "red"

    //compara se cpf contem somente caracteres numericos
    if (!cpfValor.match(numbers)) document.getElementById("spanAlert").textContent = "CPF deve conter somente caracteres numéricos."
    //verifica se o tamanho do string do cpf é igual á 11 digitos
    else if (cpfValor.length < 11) document.getElementById("spanAlert").textContent = "O CPF deve conter 11 dígitos."

    //verifica se o campo email não esta vazio
    else if (cargoValor.length <= 0) document.getElementById("spanAlert").textContent = "O Cargo não pode ser vazio."

    //verifica se o campo nome está vazio
    else if (nomeValor.length <= 0) document.getElementById("spanAlert").textContent = "O Nome não pode ser vazio"

    else {
        document.getElementById("spanAlert").textContent = "Cadastro realizado com sucesso."
        //muda a cor do span de alert para verde representando um sucesso
        document.getElementById("spanAlert").style.color = "green"

        postFuncionario()
            .catch(error => {
                console.log('Houve um erro na execução do postFuncionario')
                console.error(error)
            })
    }
}

async function postFuncionario() {
    const options = {
        method: 'POST',
        mode: "cors",
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            nome: nomeValor,
            cargo: cargoValor,
            cpf: cpfValor,
            ativo: true
        })
    };
    const response = await fetch('https://localhost:44321/api/funcionarios', options);
    const data = await response.json();

    setTimeout(function () {

       document.getElementById('nome').value = "";
         document.getElementById('cargo').value = "";
        document.getElementById('cpf').value = "";
        document.getElementById("spanAlert").textContent = ""
    }, 1500)
}

