
var funcionarioID = [];
var funcionarioNome = [];
var funcionarioCargo = [];
var funcionarioCpf = [];
var funcionarioAtivo = [];

function exibeFunc() {
    var text = '';
    var i = 0;
    if (funcionarioID.length == 0) {
        document.getElementById("spanAlertTable").textContent = "A tabela está vazia, não há nenhuma funcionario cadastrado!"
        document.getElementById("spanAlertTable").style.color = "black"
    } else {
        for (i = 0; i < funcionarioID.length; i++) {
            text += `
        
            <tr style="color:white; font-weight: bold;">
            <td style="margin-left: 2%; margin-left: 2%; background-color: #9EDB9E;">${funcionarioNome[i]}</td>
            <td style="margin-left: 2%; margin-left: 2%; background-color: #9EDB9E;">${funcionarioCargo[i]}</td>
            <td style="margin-left: 2%; margin-left: 2%; background-color: #9EDB9E;">${funcionarioCpf[i]}</td>
            <td style="margin-left: 2%; margin-left: 2%; background-color: #9EDB9E;">${funcionarioAtivo[i]}</td>  

            <td style="background-color: white;"><button style="border-radius: 10px; background-color: #1db01d; color: white; border:none; font-weight: bold; width:90%;" 
            onclick="exibirdados(${i})"> Alterar </button></td>

            <td style="background-color: white;"><button style="border-radius: 10px; background-color: #1db01d; color: white; border:none; font-weight: bold; width:90%;"
            onclick="deleteFuncionario(${i})"> Deletar </button></td>
            </tr>`;
        }
        document.getElementById('RelatorioFunc').innerHTML += text
    }
}
async function getFuncionario() {
    const response = await fetch('https://localhost:44321/api/funcionarios');
    const data = await response.json();

    //console.log(data)
    data.forEach(element => {
        const { id, nome, cargo, cpf, ativo } = element

        funcionarioID.push(id)
        funcionarioNome.push(nome)
        funcionarioCargo.push(cargo)
        funcionarioCpf.push(cpf)
        funcionarioAtivo.push(ativo)

    });
}
getFuncionario().catch(error => {
    console.log('Houve um erro na execução do getUsuarioQuestionarios')
    console.error(error)
}
)

setTimeout(function () {
    exibeFunc();
}, 1000)

function exibirdados(i) {
    var answer = window.confirm("Você tem certeza que quer alterar o cadastro do " + funcionarioNome[i] + "?  Obs: Ok = Sim e Cancel = Não");
    if (answer) {
        document.getElementById('id').value = funcionarioID[i];
        document.getElementById('nome').value = funcionarioNome[i];
        document.getElementById('cargo').value = funcionarioCargo[i];
        document.getElementById('cpf').value = funcionarioCpf[i];
        document.getElementById('ativo').value = funcionarioAtivo[i];
    }
}

async function putFuncionario() {
    const string = document.getElementById('ativo').value;
    !!string;

    const options = {
        method: 'PUT',
        mode: "cors",
        headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/json'
        },
        body: JSON.stringify({
            id: parseInt(document.getElementById('id').value),
            nome: document.getElementById('nome').value,
            cargo: document.getElementById('cargo').value,
            cpf: document.getElementById('cpf').value,
            ativo: Boolean(string),
            funcionarioProjeto: null,
        })
    };
    const response = await fetch(`https://localhost:44321/api/funcionarios/${parseInt(document.getElementById('id').value)}`, options);

    document.getElementById("spanAlert").textContent = "Modificação feita com sucesso!"
    document.getElementById("spanAlert").style.color = "green"

    setTimeout(function () {
        window.location.reload();
    }, 1250)

}

async function deleteFuncionario(i) {
    var answer = window.confirm("Você tem certeza que quer deletar o cadastro do " + funcionarioNome[i] + "?  Obs: Ok = Sim e Cancel = Não");
    if (answer) {
        const options = {
            method: 'DELETE',
            mode: "cors",
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json'
            },

        };
        const response = fetch(`https://localhost:44321/api/funcionarios/${parseInt(funcionarioID[i])}`, options);

        setTimeout(function () {
            window.location.reload();
        }, 100)
    }
    else {
        //some code
    }



}